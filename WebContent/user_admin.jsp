<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="jquery.pagination.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理</title>
<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
function suspendCheck(){

	if(window.confirm('アカウントを停止してよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}
function reactiveCheck(){
	if(window.confirm('アカウントを復活してよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}

</script>
<script type="text/javascript">
$(function() {
    $('.menu').hover(
    function(){
      $(this).animate({'marginLeft':'210px'}, 1000);
    },
    function () {
      $(this).animate({'marginLeft':'0'}, 500);
    }
  );
});

</script>
</head>
<body>
	<center>
		<div class="title">
			<h1>ユーザー管理</h1>
		</div>

		<div class="body">
		<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul style="list-style:none;">
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>


		<div>
			<table border=1 class="usersList">
				<tr bgcolor=#b2d8ff>
					<th>ログインID</th>
					<th>名前</th>
					<th>支店</th>
					<th>部署･役職</th>
					<th>停止･復活</th>
					<th>アカウント状況</th>
					<th>編集</th>
				</tr>

				<c:forEach var="user" items="${users}">
					<tr>
						<td><c:out value="${user.account}" /></td>
						<td><c:out value="${user.name}" /></td>
						<td><c:out value="${user.branchName}" /></td>
						<td><c:out value="${user.departmentName}" /></td>

						<td align="center">
						<c:if test="${user.isDeleted == 0 && user.id != loginUser.id}">
							<form action="user_admin" method="post" onSubmit="return suspendCheck()">
								<input type="hidden" value="${loginUser.id}" name="loginUserId">
								<input type="hidden" value="${user.id}" name="userId"></input>
								<input type="hidden" value="1" name="isDeleted"></input>
								<input type="submit" value="停止" id="suspend">
							</form>
						</c:if>

						<c:if test="${user.isDeleted == 1  && user.id != loginUser.id}">
						<form action="user_admin" method="post" onSubmit="return reactiveCheck()">
							<input type="hidden" value="${loginUser.id}" name="loginUserId">
							<input type="hidden" value="${user.id}" name="userId"></input>
							<input type="hidden" value="0" name="isDeleted"></input>
							<input type="submit" value="復活">
						</form>
						</c:if>
						<c:if test="${user.id == loginUser.id}">
							ログイン中
						</c:if>
						</td>
						<td align="center">
							<c:if test="${user.isDeleted == 0}">使用可能</c:if>
							<c:if test="${user.isDeleted == 1}"><font color="red">停止中</font></c:if>
						</td>

						<td align="center"><a href="settings?editUserId=${user.id}">編集</a></td>
					</tr>
				</c:forEach>
			</table>

		</div>

		<div class="menu">
			<a href="./" class="square_btn">ホームへ戻る</a>
			<a href="signup" class="square_btn">ユーザー新規登録</a>
		</div>

		<jsp:include page="copyright.jsp" flush="true" />
		</div>

	</center>

</body>
</html>