<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>
	<center>
		<div class="main-contents">

			<div class="title">
				<h1>ログイン</h1>
			</div>

			<div class="body">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul style="list-style:none;">
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="login" method="post">
				<br />
				<table id="login">
					<tr>
						<td><label for="account">ログインID</label></td>
						<td><input name="account" type="text" id="account" value="${account}"/></td>
					</tr>
					<tr>
						<td><label for="password">パスワード</label></td>
						<td><input name="password" type="password" id="password" /></td>
					</tr>

					<tr>
						<td colspan=2 align=center><input type="submit" value="ログイン" /><br /></td>
					</tr>
				</table>

			</form>
			<jsp:include page="copyright.jsp" flush="true" />
			</div>
		</div>
	</center>
</body>
</html>