<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
$(function() {
    $('.menu').hover(
    function(){
      $(this).animate({'marginLeft':'210px'}, 1000);
    },
    function () {
      $(this).animate({'marginLeft':'0'}, 500);
    }
  );
});

</script>
</head>
<body>
<center>
	<div class="title">
		<h1>新規投稿</h1>
	</div>

	<div class="body">
	<div class="errorMessages">
		<c:if test="${ not empty errorMessages }">
			<ul style="list-style:none;">
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" /></li>
				</c:forEach>
			</ul>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	</div>

	<table>
	<div class="form-area">
		<form action="newpost" method="post">
			<tr><td>件名：<br />
			<input type="text" name="title" size=55 value="${postInfo.title}">（30文字以下）</input><br />

			カテゴリー：<br />
			<input type="text" name="category" size=20 value="${postInfo.category}">（10文字以下）</input><br />

			本文：<br />
			<textarea name="text" wrap="soft" id="newpost-text"><c:out value="${postInfo.text}" /></textarea><br />
			<div align="right">（1000文字以下）<br /></div>


			</td></tr>

			<td align="center"><input type="submit" value="投稿する"></td>

		</form>
	</div>
	</table>
		<div class="menu">
			<a href="./" class="square_btn">ホームへ戻る</a>
		</div>
	<jsp:include page="copyright.jsp" flush="true" />
	</div>
</center>
</body>
</html>