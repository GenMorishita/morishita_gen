<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
<script src="js/jquery-3.3.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.timeago.js"></script>
<script type="text/javascript">
function check(){
	if(window.confirm('削除してよろしいですか？')){ // 確認ダイアログを表示
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止
	}
}
</script>
<script>
  $(function() {
	$("#datepicker1").datepicker();
	$("#datepicker1").datepicker("option", "dateFormat", 'yy/mm/dd');
  });
</script>
<script>
  $(function() {
	$("#datepicker2").datepicker();
	$("#datepicker2").datepicker("option", "dateFormat", 'yy/mm/dd');
  });
</script>
<script>
$(function() {
    $('.menu').hover(
    function(){
      $(this).stop(true).animate({'marginLeft':'210px'}, 1000);
    },
    function () {
      $(this).stop(true).animate({'marginLeft':'0'}, 500);
    }
  );
});
</script>
<script>
$(function() {
    $('#search-btn').click(function(){
        $('.search').slideToggle();
    });
});
</script>
<script>
$(function() {
	$(".show-comment").click(function() {
		$(".comment-list").slideToggle("slow");
	});
});
</script>
<script>
$(function () {
  $(".comment-box").keyup(function(){
    var counter = $(this).val().length;
    $("#countUp").text("現在" + counter + "文字");

    if(counter == 0){
      $("#countUp").text("現在" + "0" + "文字");
    }
    if(counter >= 500){
      $("#countUp").css("color","red");
    } else {
      $("#countUp").css("color","#666");
    }
  });
});
</script>
<script>
$(".timeago").ready(function(){
 $('abbr.timeago').timeago();
});
</script>

</head>

<body>
<center>
	<div class="main-contents">

		<div class="title">
				<h1>社内掲示板</h1>
		</div>

		<div class="top-body">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul style="list-style:none;">
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
				<c:remove var="errorMessages" scope="session" />
			</div>
		</c:if>

		<div class="menu">
			<button class="square_btn" id="search-btn">投稿検索</button>
			<a href="newpost" class="square_btn" id="newPostLink">新規投稿</a><br />
				<c:if test="${loginUser.departmentId == 1}">
					<a href="user_admin" class="square_btn" id="user_adminLink">ユーザー管理</a><br />
				</c:if>
			<a href="logout" class="square_btn" id="logoutLink">ログアウト</a>
		</div>

		<c:if test="${ not empty loginUser }">
			<div class="name">
				<h3>
					<c:out value="ようこそ ${loginUser.name} さん" />
				</h3>
			</div>
		</div>
			<div class="post-comment">

				<div class="search">
					<form action="./" method="get">
						<table border=2 style="margin-bottom:30px" class="search">
							<tr>投稿検索</tr>
							<tr>
								<td>カテゴリー検索：</td>
								<td><input type="text" name="category-search" value="${searchPost.categorySearch}"></input></td>
							</tr>
							<tr>
								<td>日付検索：</td>
								<td>
									<input type="text" name="start-date" value="${searchPost.startDate}" id="datepicker1"></input> ～
									<input type="text" name="end-date" value="${searchPost.endDate}" id="datepicker2"></input>
								</td>
							</tr>
							<tr>
								<td colspan=2 align="right"><input type="submit" value="検索"></input></td>
							</tr>
						</table>
					</form>
				</div>

				<h2>
					<c:out value="投稿一覧" />
				</h2>

				<c:forEach items="${posts}" var="post">
					<table border=2 style="margin-bottom:30px" class="posts-and-comments">
						<div class="post-head">
							<tr bgcolor=#b2d8ff>
								<td colspan="2"><div class="name">
								投稿者：<c:out value="${post.name}" />
								</td>

								<div class="date">
									<td colspan="1">投稿日時：<abbr title="2018-05-31T13:22:17Z+09" class="timeago"><fmt:formatDate value="${post.created_date}"
											pattern="yyyy/MM/dd HH:mm" /></abbr></td>
								</div>
							</tr>
						</div>

						<div class="title">
							<tr>
								<td colspan="3">
								件名：<c:out value="${post.title}" />
								</td>
							</tr>
						</div>
						<div class="category">
							<tr>
								<td colspan="3">
								カテゴリー：<c:out value="${post.category}" />
								</td>
							</tr>
						</div>
						<div class="post-body">
							<tr>
								<td colspan="3">本文：<br />
									<c:forEach var="s" items="${fn:split(post.text, '
									')}">
										<div><c:out value="${s}" /></div>
									</c:forEach>
								</td>
							</tr>
						</div>

						<div class="deletePost">
							<tr>
								<c:if test="${post.userId == loginUser.id}">
									<td align="right" colspan=3>
										<form action="index.jsp" method="post" onSubmit="return check()">
											<input name="postId" value="${post.id}" type="hidden"></input>
											<input name="postUserId" value="${post.userId}" type="hidden"></input>
											<input name="loginUserId" value="${loginUser.id}" type="hidden"></input>

											<input name="deletePost" type="submit" value="投稿削除"></input>
										</form>
									</td>
								</c:if>
							</tr>
						</div>

							<c:if test="${comments != null}">
								<tr><td colspan=3>
								<button class="show-comment">コメントを表示</button>
								</td></tr>
							</c:if>

						<c:forEach items="${comments}" var="comment">

							<c:if test="${post.id == comment.postId}">

								<tr bgcolor="#d8ffb2" class="comment-list">
									<div class="comment-name">
										<td colspan=2><div class="comment-name">
										from：<c:out value="${comment.name}" />
										</td>
									</div>
									<div class="comment-date">
										<td>コメント日時：<fmt:formatDate value="${comment.created_date}"
												pattern="yyyy/MM/dd HH:mm" />
										</td>
									</div>
								</tr>

								<tr class="comment-list">
									<div class="comments">
										<td colspan="3"><div class="comment">コメント：<br />
											<c:forEach var="s" items="${fn:split(comment.text, '
											')}">
												<div><c:out value="${s}" /></div>
											</c:forEach>
										</td>
									</div>
								</tr>


								<div class="deleteComment">
									<tr class="comment-list">
										<c:if test="${comment.userId == loginUser.id}">
											<form action="index.jsp" method="post" onSubmit="return check()">
												<input name="commentId" value="${comment.id}" type="hidden"></input>
												<input name="commentUserId" value="${comment.userId}" type="hidden"></input>
												<input name="loginUserId" value="${loginUser.id}" type="hidden"></input>
												<td align="right" colspan=3>
												<input name="deleteComment" type="submit" value="コメント削除"></input>
												</td>
											</form>
										</c:if>
									</tr>
								</div>
							</c:if>
						</c:forEach>

						<div class="newComment">
							<tr bgcolor="#ffffb2">
								<td colspan="3">
									<form action="newcomment" method="post">
										コメント投稿：<br />
										<textarea name="newCommentText" class="comment-box"id="newComment"><c:if test="${post.id == commentInfo.postId}"><c:out value="${commentInfo.text}" /></c:if></textarea><br />
										<input name="postId" value="${post.id}" type="hidden"></input>
										<input name="commentPostId" value="${post.id}" type="hidden"></input>
										<input name="commentUserId" value="${comment.userId}" type="hidden"></input>
										<input type="submit" value="コメントする"></input>（500文字以下）
										<!-- <p id="countUp">現在 0 文字</p> -->
									</form>
								</td>
							</tr>
						</div>
					</table>
				</c:forEach>
			</div>
		</c:if>
		<jsp:include page="copyright.jsp" flush="true" />
	</div>
</center>
</body>
</html>