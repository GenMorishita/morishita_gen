<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${editUser.name}の編集</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
$(function() {
    $('.menu').hover(
    function(){
      $(this).animate({'marginLeft':'210px'}, 1000);
    },
    function () {
      $(this).animate({'marginLeft':'0'}, 500);
    }
  );
});

</script>
</head>
<body>
	<center>
		<div class="title">
			<h1>${editUser.name}の編集</h1>
		</div>
		<div class="main-contents">

		<div class="body">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul style="list-style:none;">
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="settings" method="post">
				<br />
				<table>

					<tr>
						<td><label for="name">名前:</label></td>
						<td><input name="name" type="text"
							value="${editUser.name}" /></td>
						<td>（10文字以下）</td>
					</tr>
					<tr>
						<td><label for="account">ログインID:</label></td>
						<td><input name="account" type="text"
							value="${editUser.account}" /></td>
						<td>（半角英数字6文字以上20文字以下）</td>
					</tr>
					<tr>
						<td><label for="password">パスワード:</label></td>
						<td><input name="password" type="password" /></td>
						<td>（半角文字6文字以上20文字以下）</td>
					</tr>
					<tr>
						<td><label for="comfirmPassword">確認用パスワード:</label></td>
						<td><input name="comfirmPassword" type="password" /></td>
					</tr>


					<tr>
						<td>支店:</td>
						<td><select id="select_branch" name="branchId"
							<c:if test="${editUser.id == loginUser.id}">
							disabled
							</c:if>>
								<c:forEach var="branch" items="${branches}">

									<option value="${branch.id}"
									<c:if test="${editUser.branchId == branch.id}">
									selected
									</c:if>>
									<c:out value="${branch.name}" /></option>

								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td>部署･役職:</td>
						<td><select id="select_department" name="departmentId"
							<c:if test="${editUser.id == loginUser.id}">
							disabled
							</c:if>>
								<c:forEach var="department" items="${departments}">

									<option value="${department.id}"
									<c:if test="${editUser.departmentId == department.id}">
									selected
									</c:if>>
									<c:out value="${department.name}" /></option>

								</c:forEach>
						</select></td>
					</tr>

					<tr><td align="center" colspan=3><input type="submit" value="編集" /></td></tr>
				</table>

				<input name="id" value="${editUser.id}" id="id" type="hidden" />
				<input name="formerAccount" value="${editUser.account}" type="hidden" />

				<c:if test="${editUser.id == loginUser.id}">
					<input name="branchId" value="${loginUser.branchId}" type="hidden" />
					<input name="departmentId" value="${loginUser.departmentId}" type="hidden" />
				</c:if>

				<div class="menu">
					<a href="user_admin" class="square_btn">ユーザー管理へ戻る</a>
				</div>

			</form>
			<jsp:include page="copyright.jsp" flush="true" />
		</div>
		</div>
	</center>
</body>
</html>