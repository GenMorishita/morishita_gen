package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(filterName="loginFilter", urlPatterns = "/*")
public class LoginFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		User loginUser = (User) session.getAttribute("loginUser");

		String[] loginUrl ={"/index.jsp", "/newpost", "/user_admin", "/settings", "/signup"};
		List LoginUrlList = Arrays.asList(loginUrl);
		String LoginServletPath = ((HttpServletRequest) request).getServletPath();

		if(LoginUrlList.contains(LoginServletPath) && loginUser == null){

			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("login");
			return;
		}

		String[] accessUrl ={"/user_admin", "/settings", "/signup"};
		List AccessUrlList = Arrays.asList(accessUrl);
		String AccessServletPath = ((HttpServletRequest) request).getServletPath();

		if(AccessUrlList.contains(AccessServletPath) && loginUser.getDepartmentId() != 1){

			List<String> messages = new ArrayList<String>();
			messages.add("アクセス権限がありません");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./");
			return;
		}

		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	public void destroy() {
	}
}
