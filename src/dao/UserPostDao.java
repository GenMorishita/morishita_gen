package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Post;
import exception.SQLRuntimeException;

public class UserPostDao {

	public List<Post> getUserPosts(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id, ");
			sql.append("posts.title as title, ");
			sql.append("posts.text as text, ");
			sql.append("posts.category as category, ");
			sql.append("users.account as account, ");
			sql.append("users.name as name, ");
			sql.append("posts.created_date as created_date, ");
			sql.append("posts.user_id as user_id ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE posts.created_date >= ? AND posts.created_date <= ? ");

			if(post.getCategorySearch() != ""){
				sql.append("AND category LIKE ? ");
				sql.append("ORDER BY created_date DESC ");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, post.getStartDate());
				ps.setString(2, post.getEndDate());
				ps.setString(3, "%" + post.getCategorySearch() + "%");
			}else{
				sql.append("ORDER BY created_date DESC ");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, post.getStartDate());
				ps.setString(2, post.getEndDate());
			}

			ResultSet rs = ps.executeQuery();
			List<Post> ret = toUserPostList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Post> toUserPostList(ResultSet rs)
			throws SQLException {

		List<Post> ret = new ArrayList<Post>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				String account = rs.getString("account");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int userId = rs.getInt("user_id");

				Post post = new Post();
				post.setId(id);
				post.setTitle(title);
				post.setText(text);
				post.setCategory(category);
				post.setAccount(account);
				post.setName(name);
				post.setCreated_date(createdDate);
				post.setUserId(userId);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}