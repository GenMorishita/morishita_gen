package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class UserPostCommentDao {

	public List<Comment> getUserComments(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("users.name as name, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_date as created_date, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.post_id as post_id ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("INNER JOIN posts ");
			sql.append("ON comments.post_id = posts.id ");
			sql.append("ORDER BY created_date");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Comment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int userId = rs.getInt("user_id");
				int postId = rs.getInt("post_id");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setName(name);
				comment.setText(text);
				comment.setCreated_date(createdDate);
				comment.setUserId(userId);
				comment.setPostId(postId);

				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}