package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String account;
	private String password;
	private String comfirmPassword;
	private String name;
	private int branchId;
	private String branchName;
	private int departmentId;
	private String departmentName;
	private int isDeleted;
	private Date createdDate;
	private Date updatedDate;

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id = id;
	}

	public String getAccount(){
		return account;
	}

	public void setAccount(String account){
		this.account = account;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getComfirmPassword(){
		return comfirmPassword;
	}

	public void setComfirmPassword(String comfirmPassword){
		this.comfirmPassword = comfirmPassword;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}
	public int getBranchId(){
		return branchId;
	}

	public void setBranchId(int branchId){
		this.branchId = branchId;
	}

	public String getBranchName(){
		return branchName;
	}

	public void setBranchName(String branchName){
		this.branchName = branchName;
	}

	public int getDepartmentId(){
		return departmentId;
	}

	public void setDepartmentId(int departmentId){
		this.departmentId = departmentId;
	}

	public String getDepartmentName(){
		return departmentName;
	}

	public void setDepartmentName(String departmentName){
		this.departmentName = departmentName;
	}

	public int getIsDeleted(){
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted){
		this.isDeleted = isDeleted;
	}

	public Date getCreatedDate(){
		return createdDate;
	}

	public void setCreatedDate(Date createdDate){
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate(){
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate){
		this.updatedDate = updatedDate;
	}

}