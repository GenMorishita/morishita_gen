package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.Department;
import dao.BranchDepartmentDao;

public class BranchDepartmentService {

	public List<Branch> getBranch() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDepartmentDao branchDepartmentDao = new BranchDepartmentDao();
			List<Branch> ret = branchDepartmentDao.getBranch(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Department> getDepartment() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDepartmentDao branchDepartmentDao = new BranchDepartmentDao();
			List<Department> ret = branchDepartmentDao.getDepartment(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}