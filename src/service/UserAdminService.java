package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserAdminDao;

public class UserAdminService {

	public List<User> getUser() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserAdminDao UserAdminDao = new UserAdminDao();
			List<User> ret = UserAdminDao.getUsers(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}