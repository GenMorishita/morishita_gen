package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/newcomment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		Comment commentInfo = getCommentInfo(request);

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setText(request.getParameter("newCommentText"));
			comment.setUserId(user.getId());
			comment.setPostId(Integer.parseInt(request.getParameter("postId")));

			new CommentService().register(comment);

			response.sendRedirect("./");

		}else {

			session.setAttribute("errorMessages", messages);
			session.setAttribute("commentInfo", commentInfo);
//			request.getRequestDispatcher("./").forward(request, response);
			response.sendRedirect("./");
		}
	}

	private Comment getCommentInfo(HttpServletRequest request)
			throws IOException, ServletException {

		Comment commentInfo = new Comment();
		commentInfo.setText(request.getParameter("newCommentText"));
		commentInfo.setPostId(Integer.parseInt(request.getParameter("commentPostId")));

		return commentInfo;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String comment = request.getParameter("newCommentText");

		if (StringUtils.isBlank(comment) == true) {
			messages.add("コメントを入力してください");
		}else if (comment.length() > 500) {
			messages.add("コメントを500文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}