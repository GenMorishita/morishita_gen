package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserAdminService;
import service.UserService;

@WebServlet(urlPatterns = { "/user_admin" })
public class UserAdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<User> users = new UserAdminService().getUser();

		HttpSession session = request.getSession();

		session.setAttribute("users", users);

		request.getRequestDispatcher("/user_admin.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		int loginUserId = Integer.parseInt(request.getParameter("loginUserId"));
		int userId = Integer.parseInt(request.getParameter("userId"));

		if(loginUserId != userId){

			User user = new User();
			user.setId(Integer.parseInt(request.getParameter("userId")));
			user.setIsDeleted(Integer.parseInt(request.getParameter("isDeleted")));

			new UserService().suspendUser(user);

			response.sendRedirect("user_admin");
		}else{
			messages.add("自分のアカウントを停止･復活することは出来ません");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("user_admin");
		}


	}
}