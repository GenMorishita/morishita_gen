package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchDepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branches = new BranchDepartmentService().getBranch();
    	List<Department> departments = new BranchDepartmentService().getDepartment();

    	request.setAttribute("branches", branches);
    	request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User signupUser = getSignupUser(request);

		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {

			User user = new User();
			user.setName(request.getParameter("name"));
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setComfirmPassword(request.getParameter("comfirmPassword"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

			new UserService().register(user);

			response.sendRedirect("user_admin");

		} else {
			List<Branch> branches = new BranchDepartmentService().getBranch();
	    	List<Department> departments = new BranchDepartmentService().getDepartment();

	    	request.setAttribute("branches", branches);
	    	request.setAttribute("departments", departments);

			session.setAttribute("errorMessages", messages);
			request.setAttribute("signupUser", signupUser);
			request.getRequestDispatcher("signup.jsp").forward(request, response);

		}
	}

	private User getSignupUser(HttpServletRequest request)
            throws IOException, ServletException {

        User signupUser = new User();
        signupUser.setAccount(request.getParameter("account"));
        signupUser.setName(request.getParameter("name"));
        signupUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        signupUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return signupUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String comfirmPassword = request.getParameter("comfirmPassword");
		int branchId = (Integer.parseInt(request.getParameter("branchId")));
		int departmentId = (Integer.parseInt(request.getParameter("departmentId")));

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}else if (name.length() > 10) {
			messages.add("名前を10文字以下で入力してください");
		}

		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		}else if (account.length() < 6 || account.length() > 20 || !account.matches("^[a-zA-Z0-9]+$")) {
			messages.add("ログインIDを半角英数字6文字以上20文字以下で入力してください");
		}

		User checkAccount = new UserService().checkAccount(account);

		if(checkAccount != null){
			messages.add("このログインIDは既に使用されています");
		}

		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}else if (password.length() < 6 || password.length() > 20) {
			messages.add("パスワードを半角文字6文字以上20文字以下で入力してください");
		}

		if (StringUtils.isBlank(comfirmPassword) == true) {
			messages.add("確認用パスワードを入力してください");
		}else if (!password.equals(comfirmPassword)) {
			messages.add("パスワードが一致しません");
		}

		if (branchId == 0) {
			messages.add("支店名を選択してください");
		}

		if (departmentId == 0) {
			messages.add("部署･役職名を選択してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}