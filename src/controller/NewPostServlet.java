package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("/newpost.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		Post postInfo = getPostInfo(request);

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Post post = new Post();
			post.setTitle(request.getParameter("title"));
			post.setText(request.getParameter("text"));
			post.setCategory(request.getParameter("category"));
			post.setAccount(request.getParameter("account"));
			post.setName(request.getParameter("name"));
			post.setUserId(user.getId());

			new PostService().register(post);

			response.sendRedirect("./");
		}else {

			request.setAttribute("errorMessages", messages);
			request.setAttribute("postInfo", postInfo);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}
	}

	private Post getPostInfo(HttpServletRequest request)
            throws IOException, ServletException {

		Post postInfo = new Post();
		postInfo.setTitle(request.getParameter("title"));
		postInfo.setCategory(request.getParameter("category"));
		postInfo.setText(request.getParameter("text"));

		return postInfo;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}else if (title.length() > 30) {
			messages.add("件名を30文字以下で入力してください");
		}

		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}else if (category.length() > 10) {
			messages.add("カテゴリーを10文字以下で入力してください");
		}

		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}else if (text.length() > 1000) {
			messages.add("本文を1000文字以下で入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}