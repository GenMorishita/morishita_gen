package controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Post;
import service.CommentService;
import service.PostService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Post post = new Post();

		Post searchPost = getSearchPost(request);

		if(request.getParameter("category-search") == null){
			post.setCategorySearch("");
		}else{
			post.setCategorySearch(request.getParameter("category-search"));
			request.setAttribute("category", post.getCategorySearch());
		}

		if(StringUtils.isBlank(request.getParameter("start-date"))){
			String stDate = "2017/05/01 00:00:00";
			post.setStartDate(stDate);
		}else{
			post.setStartDate(request.getParameter("start-date"));
		}

		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		Calendar calendar = Calendar.getInstance();

		if(StringUtils.isEmpty(request.getParameter("end-date"))){
			calendar.setTime(date);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			Date tomorrow = calendar.getTime();
			String edDate = df.format(tomorrow);
			post.setEndDate(edDate);
		}else{
			Date endDate = null;
			try {
				endDate = df.parse(request.getParameter("end-date"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			calendar.setTime(endDate);
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			Date nextDay = calendar.getTime();
			String edDate = df.format(nextDay);
			post.setEndDate(edDate);
		}

		request.setAttribute("searchPost", searchPost);

		List<Post> posts = new PostService().getPost(post);
		request.setAttribute("posts", posts);

		List<Comment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
		//		response.sendRedirect("./");
	}

	private Post getSearchPost(HttpServletRequest request)
			throws IOException, ServletException {

		Post searchPost = new Post();
		searchPost.setCategorySearch(request.getParameter("category-search"));
		searchPost.setStartDate(request.getParameter("start-date"));
		searchPost.setEndDate(request.getParameter("end-date"));

		return searchPost;
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		int loginUserId = Integer.parseInt(request.getParameter("loginUserId"));

		if(request.getParameter("postId") != null){

			int postUserId = Integer.parseInt(request.getParameter("postUserId"));

			Post post = new Post();
			post.setId(Integer.parseInt(request.getParameter("postId")));

			if(loginUserId == postUserId){

				new PostService().delete(post);

				response.sendRedirect("./");
				return;
			}else{
				messages.add("他の人の投稿を削除することは出来ません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("./");
				return;
			}
		}

		if(request.getParameter("commentId") != null){

			int commentUserId = Integer.parseInt(request.getParameter("commentUserId"));

			Comment comment = new Comment();
			comment.setId(Integer.parseInt(request.getParameter("commentId")));

			if(loginUserId == commentUserId){

				new CommentService().delete(comment);

				response.sendRedirect("./");
				return;
			}else{
				messages.add("他の人のコメントを削除することは出来ません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("./");
			}
		}
	}
}