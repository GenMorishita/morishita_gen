package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchDepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	List<String> messages = new ArrayList<String>();

    	List<Branch> branches = new BranchDepartmentService().getBranch();
    	List<Department> departments = new BranchDepartmentService().getDepartment();

    	request.setAttribute("branches", branches);
    	request.setAttribute("departments", departments);

        //編集するユーザーのidを取得
        String editUserId = (String) request.getParameter("editUserId");

        if(!editUserId.matches("^[0-9]+") || editUserId == null){
        	HttpSession session = request.getSession();

        	messages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("user_admin");
        	return;
        }

        //編集するユーザーのidを元にDBからユーザー情報取得
        User editUser = new UserService().getUser(Integer.parseInt(editUserId));

        request.setAttribute("editUser", editUser);

        if(editUser == null){

        	HttpSession session = request.getSession();

        	messages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", messages);
        	response.sendRedirect("user_admin");
        	return;
        }

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }

            response.sendRedirect("user_admin");
        } else {
        	List<Branch> branches = new BranchDepartmentService().getBranch();
        	List<Department> departments = new BranchDepartmentService().getDepartment();

        	request.setAttribute("branches", branches);
        	request.setAttribute("departments", departments);

            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setAccount(request.getParameter("account"));
        editUser.setComfirmPassword(request.getParameter("comfirmPassword"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

        if(request.getParameter("password") != ""){
        	editUser.setPassword(request.getParameter("password"));
        }

        return editUser;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String comfirmPassword = request.getParameter("comfirmPassword");
		int branchId = (Integer.parseInt(request.getParameter("branchId")));
		int departmentId = (Integer.parseInt(request.getParameter("departmentId")));
		String formerAccount = request.getParameter("formerAccount");

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}else if (name.length() > 10) {
			messages.add("名前を10文字以下で入力してください");
		}

		if (StringUtils.isBlank(account) == true) {
			messages.add("ログインIDを入力してください");
		}else if (account.length() < 6 || account.length() > 20 || !account.matches("^[a-zA-Z0-9]+$")) {
			messages.add("ログインIDを半角英数字6文字以上20文字以下で入力してください");
		}

		User checkAccountInfo = new UserService().checkAccount(account);

		if(!account.equals(formerAccount) && checkAccountInfo != null){
			messages.add("このログインIDは既に使用されています");
		}

		if (StringUtils.isBlank(password) != true && password.length() < 6 || password.length() > 20) {
			messages.add("パスワードを半角文字6文字以上20文字以下で入力してください");
		}

		if (StringUtils.isBlank(password) != true && StringUtils.isBlank(comfirmPassword) == true) {
			messages.add("確認用パスワードを入力してください");
		}else if (StringUtils.isBlank(password) != true && !password.equals(comfirmPassword)) {
			messages.add("パスワードが一致しません");
		}

		if (branchId == 0) {
			messages.add("支店名を選択してください");
		}

		if (departmentId == 0) {
			messages.add("部署･役職名を選択してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}